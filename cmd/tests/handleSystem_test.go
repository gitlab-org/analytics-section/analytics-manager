package tests

import (
	"net/http"
	"net/http/httptest"
	"strconv"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestPendingMigrations(t *testing.T) {
	pendingMigrations := 3
	srv, testDB := setupTestServerWithDB()

	testDB.On("GetPendingMigrations").Return(pendingMigrations, nil)

	resp := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "/system/pending_migrations", nil)
	req.SetBasicAuth("user", "pass")
	srv.Gin.ServeHTTP(resp, req)

	assert.Equal(t, 200, resp.Code)
	expectedBody := `{"pendingMigrations":` + strconv.Itoa(pendingMigrations) + `}`
	assert.Equal(t, expectedBody, resp.Body.String())

	testDB.AssertExpectations(t)
}
