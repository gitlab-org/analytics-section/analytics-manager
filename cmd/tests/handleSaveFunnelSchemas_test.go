package tests

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"
)

func TestSaveFunnelSchemasForProject(t *testing.T) {
	schemaProject1 := "[   {\"name\": \"completed_purchase\",\"sql\": \"      SELECT\n        (SELECT max(derived_tstamp) FROM gitlab_project_1.snowplow_events) as x,\n        windowFunnel(3600)(toDateTime(derived_tstamp), page_urlpath = '/page1.html', page_urlpath = '/page2.html', page_urlpath = '/page3.html') as step\n        FROM gitlab_project_1.snowplow_events\n\",\"steps\": [\"page_urlpath = '/page1.html'\",\"page_urlpath = '/page2.html'\",\"page_urlpath = '/page3.html'\"]}]"
	schemaProject2 := "[   {\"name\": \"completed_purchase\",\"sql\": \"      SELECT\n        (SELECT max(derived_tstamp) FROM gitlab_project_2.snowplow_events) as x,\n        windowFunnel(3600)(toDateTime(derived_tstamp), page_urlpath = '/page1.html', page_urlpath = '/page2.html', page_urlpath = '/page3.html') as step\n        FROM gitlab_project_2.snowplow_events\n\",\"steps\": [\"page_urlpath = '/page1.html'\",\"page_urlpath = '/page2.html'\",\"page_urlpath = '/page3.html'\"]}]"

	schemaV2Project1 := "[   {\"name\": \"completed_purchase\",\"sql\": \"      SELECT\n        (SELECT max(derived_tstamp) FROM gitlab_project_1.snowplow_events) as x,\n        windowFunnel(3600)(toDateTime(derived_tstamp), page_urlpath = '/page1.html', page_urlpath = '/page2.html', page_urlpath = '/page3.html') as step\n        FROM gitlab_project_1.snowplow_events\n\",\"steps\": [\"page_urlpath = '/page1_v2.html'\",\"page_urlpath = '/page2_v2.html'\",\"page_urlpath = '/page3_v2.html'\"]}]"
	schemaV2Project2 := "[   {\"name\": \"completed_purchase\",\"sql\": \"      SELECT\n        (SELECT max(derived_tstamp) FROM gitlab_project_2.snowplow_events) as x,\n        windowFunnel(3600)(toDateTime(derived_tstamp), page_urlpath = '/page1.html', page_urlpath = '/page2.html', page_urlpath = '/page3.html') as step\n        FROM gitlab_project_2.snowplow_events\n\",\"steps\": [\"page_urlpath = '/page1_v2.html'\",\"page_urlpath = '/page2_v2.html'\",\"page_urlpath = '/page3_v2.html'\"]}]"

	tests := []struct {
		name               string
		fixturePath        string
		prepareMocks       func(mockDB *mockedDB)
		expectedStatusCode int
	}{
		{
			name:        "funnel created for single project",
			fixturePath: "./fixtures/request_funnel_created_single_project.json",
			prepareMocks: func(mockDB *mockedDB) {
				mockDB.On("DeleteFunnelSchema", "gitlab_project_1", "completed_purchase").Return(nil)
				mockDB.On("InsertFunnelSchema", "gitlab_project_1", "completed_purchase", schemaProject1).Return(nil)
			},
			expectedStatusCode: 200,
		},
		{
			name:        "funnel created for multiple projects",
			fixturePath: "./fixtures/request_funnel_created_multiple_projects.json",
			prepareMocks: func(mockDB *mockedDB) {
				mockDB.On("DeleteFunnelSchema", "gitlab_project_1", "completed_purchase").Return(nil)
				mockDB.On("DeleteFunnelSchema", "gitlab_project_2", "completed_purchase").Return(nil)

				mockDB.On("InsertFunnelSchema", "gitlab_project_1", "completed_purchase", schemaProject1).Return(nil)
				mockDB.On("InsertFunnelSchema", "gitlab_project_2", "completed_purchase", schemaProject2).Return(nil)
			},
			expectedStatusCode: 200,
		},
		{
			name:        "funnel could not be created",
			fixturePath: "./fixtures/request_funnel_created_single_project.json",
			prepareMocks: func(mockDB *mockedDB) {
				mockDB.On("DeleteFunnelSchema", "gitlab_project_1", "completed_purchase").Return(nil)
				mockDB.On("InsertFunnelSchema", "gitlab_project_1", "completed_purchase", schemaProject1).Return(fmt.Errorf("could not insert funnel"))
			},
			expectedStatusCode: 500,
		},
		{
			name:        "funnel updated",
			fixturePath: "./fixtures/request_funnel_updated.json",
			prepareMocks: func(mockDB *mockedDB) {
				mockDB.On("DeleteFunnelSchema", "gitlab_project_1", "completed_purchase").Return(nil)
				mockDB.On("InsertFunnelSchema", "gitlab_project_1", "completed_purchase", schemaV2Project1).Return(nil)
			},
			expectedStatusCode: 200,
		},
		{
			name:        "funnel updated and renamed",
			fixturePath: "./fixtures/request_funnel_updated_and_renamed.json",
			prepareMocks: func(mockDB *mockedDB) {
				mockDB.On("DeleteFunnelSchema", "gitlab_project_1", "completed_purchase").Return(nil)
				mockDB.On("DeleteFunnelSchema", "gitlab_project_1", "completed_purchase_v2").Return(nil)
				mockDB.On("InsertFunnelSchema", "gitlab_project_1", "completed_purchase_v2", schemaV2Project1).Return(nil)
			},
			expectedStatusCode: 200,
		},
		{
			name:        "funnel could not be updated",
			fixturePath: "./fixtures/request_funnel_updated.json",
			prepareMocks: func(mockDB *mockedDB) {
				mockDB.On("DeleteFunnelSchema", "gitlab_project_1", "completed_purchase").Return(nil)
				mockDB.On("InsertFunnelSchema", "gitlab_project_1", "completed_purchase", schemaV2Project1).Return(fmt.Errorf("could not insert funnel"))
			},
			expectedStatusCode: 500,
		},
		{
			name:        "funnel deleted",
			fixturePath: "./fixtures/request_funnel_deleted.json",
			prepareMocks: func(mockDB *mockedDB) {
				mockDB.On("DeleteFunnelSchema", "gitlab_project_1", "completed_purchase").Return(nil)
			},
			expectedStatusCode: 200,
		},
		{
			name:        "funnel could not be deleted",
			fixturePath: "./fixtures/request_funnel_deleted.json",
			prepareMocks: func(mockDB *mockedDB) {
				mockDB.On("DeleteFunnelSchema", "gitlab_project_1", "completed_purchase").Return(fmt.Errorf("could not delete funnel"))
			},
			expectedStatusCode: 500,
		},
		{
			name:        "multiple projects multiple actions",
			fixturePath: "./fixtures/request_funnels_created_updated_deleted.json",
			prepareMocks: func(mockDB *mockedDB) {
				mockDB.On("DeleteFunnelSchema", "gitlab_project_1", "completed_purchase").Return(nil)
				mockDB.On("InsertFunnelSchema", "gitlab_project_1", "completed_purchase", schemaProject1).Return(nil)
				mockDB.On("DeleteFunnelSchema", "gitlab_project_1", "completed_purchase_v2").Return(nil)
				mockDB.On("InsertFunnelSchema", "gitlab_project_1", "completed_purchase_v2", schemaV2Project1).Return(nil)
				mockDB.On("DeleteFunnelSchema", "gitlab_project_1", "completed_purchase_old").Return(nil)
				mockDB.On("DeleteFunnelSchema", "gitlab_project_1", "completed_purchase_new").Return(nil)
				mockDB.On("InsertFunnelSchema", "gitlab_project_1", "completed_purchase_new", schemaV2Project1).Return(nil)
				mockDB.On("DeleteFunnelSchema", "gitlab_project_1", "completed_purchase_deprecated").Return(nil)

				mockDB.On("DeleteFunnelSchema", "gitlab_project_2", "completed_purchase").Return(nil)
				mockDB.On("InsertFunnelSchema", "gitlab_project_2", "completed_purchase", schemaProject2).Return(nil)
				mockDB.On("DeleteFunnelSchema", "gitlab_project_2", "completed_purchase_v2").Return(nil)
				mockDB.On("InsertFunnelSchema", "gitlab_project_2", "completed_purchase_v2", schemaV2Project2).Return(nil)
				mockDB.On("DeleteFunnelSchema", "gitlab_project_2", "completed_purchase_old").Return(nil)
				mockDB.On("DeleteFunnelSchema", "gitlab_project_2", "completed_purchase_new").Return(nil)
				mockDB.On("InsertFunnelSchema", "gitlab_project_2", "completed_purchase_new", schemaV2Project2).Return(nil)
				mockDB.On("DeleteFunnelSchema", "gitlab_project_2", "completed_purchase_deprecated").Return(nil)
			},
			expectedStatusCode: 200,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			srv, testDB := setupTestServerWithDB()
			tt.prepareMocks(testDB)

			resp := httptest.NewRecorder()
			jsonFile, err := os.Open(tt.fixturePath)
			require.NoError(t, err)

			defer jsonFile.Close()

			req, _ := http.NewRequest("POST", fmt.Sprintf("/funnel-schemas"), jsonFile)
			req.SetBasicAuth("user", "pass")

			srv.Gin.ServeHTTP(resp, req)

			assert.Equal(t, tt.expectedStatusCode, resp.Code)
			testDB.AssertExpectations(t)
		})
	}
}
