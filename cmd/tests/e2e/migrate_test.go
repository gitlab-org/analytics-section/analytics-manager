package tests

import (
	"gitlab.com/gitlab-org/analytics-section/analytics-manager/internal/platform/clickhouse"
	"gitlab.com/gitlab-org/analytics-section/analytics-manager/internal/platform/utils"
	"log"
	"testing"

	_ "github.com/ClickHouse/clickhouse-go/v2" // clickhouse driver import
	_ "gitlab.com/gitlab-org/analytics-section/analytics-manager/internal/platform/clickhouse/migrations"
)

func TestMigrations(t *testing.T) {
	if testing.Short() {
		t.Skip("skipping e2e test")
	}

	url := utils.GetEnv("CLICKHOUSE_TEST_URL")
	username := utils.GetEnv("CLICKHOUSE_TEST_USERNAME")
	password := utils.GetEnv("CLICKHOUSE_TEST_PASSWORD")
	clickHouseDsn, err := clickhouse.FormatDsn(url, username, password)
	if err != nil {
		log.Fatal(err)
	}

	db, err := clickhouse.ConnectToDB(clickHouseDsn)
	if err = clickhouse.MigrateDb(clickHouseDsn); err != nil {
		log.Fatal(err)
	}

	log.Print("manager: deleting all the project databases in the test instance")
	dbs, err := db.DropProjectDatabases()
	if err != nil {
		log.Fatal(err)
	}
	log.Printf("manager: deleted %s project databases", dbs)

	log.Print("manager: deleting all the entities from the default database in the test instance")
	tables, err := db.CleanDefaultDb()
	if err != nil {
		log.Fatal(err)
	}
	log.Printf("manager: deleted %s tables from default DB", tables)
}
