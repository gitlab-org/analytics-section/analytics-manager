package main

import (
	"log"

	"gitlab.com/gitlab-org/analytics-section/analytics-manager/internal/platform/clickhouse"
	"gitlab.com/gitlab-org/analytics-section/analytics-manager/internal/platform/utils"

	_ "github.com/ClickHouse/clickhouse-go/v2" // clickhouse driver import

	_ "gitlab.com/gitlab-org/analytics-section/analytics-manager/internal/platform/clickhouse/migrations"
)

func main() {
	url := utils.GetEnv("CLICKHOUSE_URL")
	username := utils.GetEnv("CLICKHOUSE_USERNAME")
	password := utils.GetEnv("CLICKHOUSE_PASSWORD")
	clickHouseDsn, err := clickhouse.FormatDsn(url, username, password)
	if err != nil {
		log.Fatal(err)
	}

	if err = clickhouse.RollbackDB(clickHouseDsn); err != nil {
		log.Fatal(err)
	}
}
