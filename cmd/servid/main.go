package main

import (
	"log"

	"github.com/joho/godotenv"

	"gitlab.com/gitlab-org/analytics-section/analytics-manager/cmd/servid/handlers"
	"gitlab.com/gitlab-org/analytics-section/analytics-manager/internal/platform/clickhouse"
	"gitlab.com/gitlab-org/analytics-section/analytics-manager/internal/platform/utils"

	_ "gitlab.com/gitlab-org/analytics-section/analytics-manager/internal/platform/clickhouse/migrations"
)

func main() {
	err := godotenv.Load()

	if err != nil {
		log.Print("No .env file found, using only environment variables")
	}

	url := utils.GetEnv("CLICKHOUSE_URL")
	username := utils.GetEnv("CLICKHOUSE_USERNAME")
	password := utils.GetEnv("CLICKHOUSE_PASSWORD")
	clickHouseDsn, err := clickhouse.FormatDsn(url, username, password)
	if err != nil {
		log.Fatal(err)
	}

	// Connect to Db and ping it to make sure it's active
	db, err := clickhouse.ConnectToDB(clickHouseDsn)
	if err != nil {
		log.Fatal(err)
	}

	srv := &handlers.Server{DB: db}

	srv.InitServer()
	if err = srv.Start(":4567"); err != nil {
		log.Fatal(err)
	}
}
