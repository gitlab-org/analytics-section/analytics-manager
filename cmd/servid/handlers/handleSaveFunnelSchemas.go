package handlers

import (
	"encoding/json"
	"fmt"
	"github.com/gin-gonic/gin"
	"io"
	"log"
	"net/http"
	"regexp"
	"strings"
)

var (
	FunnelStateCreated FunnelState = "created"
	FunnelStateUpdated FunnelState = "updated"
	FunnelStateDeleted FunnelState = "deleted"

	eventsTableRegex = regexp.MustCompile(`(gitlab_project_\d+)(\.snowplow_events)`)
)

type FunnelState string

type Funnel struct {
	Name         string      `json:"name"`
	PreviousName string      `json:"previous_name"`
	Schema       string      `json:"schema"`
	State        FunnelState `json:"state"`
}

type FunnelSchemasRequestBody struct {
	ProjectIDs []string `json:"project_ids"`
	Funnels    []Funnel `json:"funnels"`
}

func (s *Server) SaveFunnelSchemas(ctx *gin.Context) {
	var b *FunnelSchemasRequestBody

	body, err := io.ReadAll(ctx.Request.Body)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, "could not read body")
		return
	}

	err = json.Unmarshal(body, &b)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, "could not unmarshal body")
		return
	}

	for _, projectID := range b.ProjectIDs {
		err = s.saveFunnelSchemasForProject(projectID, b.Funnels)
		if err != nil {
			log.Print(err)
			ctx.JSON(http.StatusInternalServerError, gin.H{"result": "error", "message": "Handling funnel schema request failed"})
			return
		}
	}

	ctx.JSON(http.StatusOK, gin.H{
		"result": "success",
	})
	return
}

func (s *Server) saveFunnelSchemasForProject(projectID string, funnels []Funnel) error {
	for _, funnel := range funnels {
		var err error
		switch funnel.State {
		case FunnelStateCreated:
			err = adjustDBNameInFunnelSchema(&funnel, projectID)
			if err != nil {
				break
			}
			err = s.createFunnelForProject(projectID, funnel)
		case FunnelStateUpdated:
			err = adjustDBNameInFunnelSchema(&funnel, projectID)
			if err != nil {
				break
			}
			err = s.updateFunnelForProject(projectID, funnel)
		case FunnelStateDeleted:
			err = s.deleteFunnelFromProject(projectID, funnel.Name)
		}

		if err != nil {
			return fmt.Errorf("could not save funnel: %w", err)
		}
	}
	return nil
}

func (s *Server) createFunnelForProject(projectID string, funnel Funnel) error {
	// since primary keys are not unique, delete funnels with the same name
	err := s.DB.DeleteFunnelSchema(projectID, funnel.Name)
	if err != nil {
		return err
	}

	return s.DB.InsertFunnelSchema(projectID, funnel.Name, funnel.Schema)
}

func (s *Server) updateFunnelForProject(projectID string, funnel Funnel) error {
	previousName := funnel.PreviousName
	// request may have missing previousName if the funnel name is not changed by the user
	if previousName == "" {
		previousName = funnel.Name
	}

	err := s.DB.DeleteFunnelSchema(projectID, previousName)
	if err != nil {
		return err
	}

	return s.createFunnelForProject(projectID, funnel)
}

func (s *Server) deleteFunnelFromProject(projectID string, funnelName string) error {
	return s.DB.DeleteFunnelSchema(projectID, funnelName)
}

func adjustDBNameInFunnelSchema(funnel *Funnel, projectID string) error {
	matches := eventsTableRegex.FindStringSubmatch(funnel.Schema)
	// matches should be an array like [gitlab_project_20.snowplow_events gitlab_project_20 .snowplow_events]
	if len(matches) != 3 {
		return fmt.Errorf("unexpected funnel schema")
	}

	funnel.Schema = strings.ReplaceAll(funnel.Schema, matches[1], projectID)

	return nil
}
