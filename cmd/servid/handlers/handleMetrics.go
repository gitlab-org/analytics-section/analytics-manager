package handlers

import (
	"github.com/gin-gonic/gin"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

func (s *Server) Metrics(ctx *gin.Context) {
	h := promhttp.Handler()

	h.ServeHTTP(ctx.Writer, ctx.Request)
}
