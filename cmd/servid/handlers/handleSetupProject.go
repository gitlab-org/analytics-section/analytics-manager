package handlers

import (
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"

	"gitlab.com/gitlab-org/analytics-section/analytics-manager/internal/platform/utils"
)

// SetupProject creates a new database and event table and view for a new project
func (s *Server) SetupProject(ctx *gin.Context) {
	projectId := ctx.Param("projectId")
	if !utils.StartsWithLetter(projectId) {
		projectId = "db_" + projectId
	}

	// Sanitize projectId to prevent SQL injection
	projectId = utils.SanitizeString(projectId)

	err := s.DB.CreateDatabase(projectId)
	if err != nil {
		log.Print(err)
		ctx.JSON(http.StatusInternalServerError, gin.H{"result": "error", "message": "Database creation failed"})
		return
	}

	err = s.DB.CreateProjectSnowplowEventsTable(projectId, s.Config.isCluster)
	if err != nil {
		log.Print(err)
		ctx.JSON(http.StatusInternalServerError, gin.H{"result": "error", "message": "error creating snowplow events table"})
		return
	}

	err = s.DB.CreateSessionsTable(projectId)
	if err != nil {
		log.Print(err)
		ctx.JSON(http.StatusInternalServerError, gin.H{"result": "error", "message": "error creating sessions table"})
		return
	}

	err = s.DB.CreatePartitionedSessionsTable(projectId)
	if err != nil {
		log.Print(err)
		ctx.JSON(http.StatusInternalServerError, gin.H{"result": "error", "message": "error creating partitioned sessions table"})
		return
	}

	err = s.DB.CreatePartitionedSessionsView(projectId)
	if err != nil {
		log.Print(err)
		ctx.JSON(http.StatusInternalServerError, gin.H{"result": "error", "message": "error creating partitioned sessions view"})
		return
	}

	err = s.DB.CreateSessionsView(projectId)
	if err != nil {
		log.Print(err)
		ctx.JSON(http.StatusInternalServerError, gin.H{"result": "error", "message": "error creating sessions view"})
		return
	}

	err = s.DB.CreateFunnelSchemasTable(projectId)
	if err != nil {
		log.Print(err)
		ctx.JSON(http.StatusInternalServerError, gin.H{"result": "error", "message": "error creating funnel_schemas table"})
		return
	}

	existingAppId, err := s.DB.GetAppIdByProjectId(projectId)
	if err != nil {
		log.Print(err)
		ctx.JSON(http.StatusInternalServerError, gin.H{"result": "error", "message": "error fetching project ID"})
		return
	}

	if existingAppId != nil {
		ctx.JSON(http.StatusOK, gin.H{"result": "info", "message": "Project ID already exists", "app_id": *existingAppId})
		return
	}

	appId := uuid.NewString()
	err = s.DB.InsertIntoActiveApps(appId, projectId)
	if err != nil {
		log.Print(err)
		ctx.JSON(http.StatusInternalServerError, gin.H{"result": "error", "message": "error inserting into active apps table"})
		return
	}

	err = s.DB.CreateProjectView(appId, projectId)
	if err != nil {
		log.Print(err)
		ctx.JSON(http.StatusInternalServerError, gin.H{"result": "error", "message": "error creating project view"})
		return

	}

	ctx.JSON(http.StatusOK, gin.H{
		"app_id":     appId,
		"project_id": projectId,
	})
}
