package handlers

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

// PendingMigrations returns the number of pending migrations in the database
func (s *Server) PendingMigrations(context *gin.Context) {
	pendingMigrations, err := s.DB.GetPendingMigrations()
	if err != nil {
		context.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	context.JSON(http.StatusOK, gin.H{"pendingMigrations": pendingMigrations})
}
