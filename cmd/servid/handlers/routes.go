package handlers

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

// RegisterRoutes registers all the routes for the API
func (s *Server) RegisterRoutes() {
	s.Gin.GET("/healthz", func(c *gin.Context) {
		c.JSON(http.StatusOK, "ok")
	})

	authorized := s.Gin.Group("/")
	authorized.Use(auth(s.Config))
	{
		authorized.GET("/system/pending_migrations", s.PendingMigrations)
		authorized.GET("/usage/:projectId/:year/:month", s.ProjectUsage)
		authorized.POST("/funnel-schemas", s.SaveFunnelSchemas)
		authorized.POST("/setup-project/:projectId", s.SetupProject)
		authorized.GET("/metrics", s.Metrics)
	}
}

func auth(config *config) gin.HandlerFunc {
	return gin.BasicAuth(gin.Accounts{
		config.auth.username: config.auth.password,
	})
}
