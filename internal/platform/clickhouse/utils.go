package clickhouse

import (
	"fmt"
	"strings"
)

// FormatDsn formats the DSN for the clickhouse driver.
func FormatDsn(url, username, password string) (string, error) {
	if url == "" {
		return "", fmt.Errorf("URL must not be empty")
	}

	if strings.HasPrefix(url, "https://") {
		return fmt.Sprintf("%s?secure=true&username=%s&password=%s", url, username, password), nil
	}
	return fmt.Sprintf("%s?username=%s&password=%s", url, username, password), nil
}
