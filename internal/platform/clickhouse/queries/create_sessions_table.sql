CREATE TABLE IF NOT EXISTS {database_name:Identifier}.sessions
(
    domain_sessionid String,
    user_id Nullable(String),
    last_timestamp SimpleAggregateFunction(max, DateTime('UTC')),
    first_timestamp SimpleAggregateFunction(min, DateTime('UTC')),
    agent_name Nullable(String),
    agent_version Nullable(String),
    browser_language Nullable(String),
    viewport_size Nullable(String),
    os_name Nullable(String),
    os_version Nullable(String),
    os_version_major Nullable(String)
)
    ENGINE = AggregatingMergeTree
        ORDER BY (domain_sessionid);
