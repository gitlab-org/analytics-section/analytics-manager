CREATE MATERIALIZED VIEW IF NOT EXISTS {database_name:Identifier}.sessions_mv TO {database_name:Identifier}.sessions
AS SELECT
    `domain_sessionid` AS domain_sessionid,
    any(`user_id`) AS user_id,
    any(visitParamExtractString(`derived_contexts`, 'agentName')) AS agent_name,
    any(visitParamExtractString(`derived_contexts`, 'agentVersion')) AS agent_version,
    any(visitParamExtractString(`contexts`, 'browserLanguage')) as browser_language,
    any(visitParamExtractString(`derived_contexts`, 'operatingSystemName')) AS os_name,
    any(visitParamExtractString(`contexts`, 'viewport')) as viewport_size,
    any(visitParamExtractString(`derived_contexts`, 'operatingSystemVersion')) as os_version,
    any(visitParamExtractString(`derived_contexts`, 'operatingSystemVersionMajor')) as os_version_major,
    maxSimpleState(`derived_tstamp`) AS last_timestamp, -- SimpleState is necessary to make the min/max functions work on all rows not only recently imported ones
    minSimpleState(`derived_tstamp`) AS first_timestamp
FROM {database_name:Identifier}.snowplow_events
WHERE user_id_type IN ('cookie', 'identify')
GROUP BY
    domain_sessionid;
