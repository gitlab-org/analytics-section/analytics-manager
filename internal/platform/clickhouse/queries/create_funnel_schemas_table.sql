CREATE TABLE IF NOT EXISTS {database_name:Identifier}.funnel_schemas
(
    name String,
    contents String,
    created_at DateTime64(3, 'GMT'),
    updated_at DateTime64(3, 'GMT')
)
    ENGINE = MergeTree
    PRIMARY KEY (name, created_at);
