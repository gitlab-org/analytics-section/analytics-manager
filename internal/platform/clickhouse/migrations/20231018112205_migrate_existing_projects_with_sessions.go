package migrations

import (
	"context"
	"database/sql"
	"fmt"

	"github.com/pressly/goose/v3"
)

func init() {
	goose.AddMigrationContext(upMigrateExistingProjectsWithSessions, downMigrateExistingProjectsWithSessions)
}

func upMigrateExistingProjectsWithSessions(ctx context.Context, tx *sql.Tx) error {
	var projectIds []string

	hasActiveAppsTable, err := activeAppsTableExists(ctx, tx)
	switch {
	case err != nil:
		return err
	case !hasActiveAppsTable:
		return nil
	}

	rows, err := tx.QueryContext(ctx, "SELECT Distinct project_id from default.active_apps")
	if err != nil {
		return err
	}
	defer rows.Close()

	for rows.Next() {
		var projectId string
		if err := rows.Scan(&projectId); err != nil {
			return err
		}
		projectIds = append(projectIds, projectId)
	}

	for _, projectId := range projectIds {
		sessionsTables := fmt.Sprintf(`CREATE TABLE IF NOT EXISTS %s.sessions (
			domain_sessionid String,
			user_id Nullable(String),
			last_timestamp SimpleAggregateFunction(
			max,
			DateTime('UTC')
		),
			first_timestamp SimpleAggregateFunction(
			min,
			DateTime('UTC')
		),
			agent_name Nullable(String),
			agent_version Nullable(String),
			browser_language Nullable(String),
			viewport_size Nullable(String),
			os_name Nullable(String),
			os_version Nullable(String),
			os_version_major Nullable(String)
		) ENGINE = AggregatingMergeTree
		ORDER BY
		(domain_sessionid);`, projectId)

		_, err := tx.ExecContext(ctx, sessionsTables)
		if err != nil {
			return err
		}

		sessionsViews := fmt.Sprintf(`CREATE MATERIALIZED VIEW IF NOT EXISTS %s.sessions_mv TO %s.sessions AS 
SELECT 
  domain_sessionid AS domain_sessionid, 
  any(user_id) AS user_id, 
  any(
    visitParamExtractString(derived_contexts, 'agentName')
  ) AS agent_name, 
  any(
    visitParamExtractString(
      derived_contexts, 'agentVersion'
    )
  ) AS agent_version, 
  any(
    visitParamExtractString(contexts, 'browserLanguage')
  ) as browser_language, 
  any(
    visitParamExtractString(
      derived_contexts, 'operatingSystemName'
    )
  ) AS os_name, 
  any(
    visitParamExtractString(contexts, 'viewport')
  ) as viewport_size, 
  any(
    visitParamExtractString(
      derived_contexts, 'operatingSystemVersion'
    )
  ) as os_version, 
  any(
    visitParamExtractString(
      derived_contexts, 'operatingSystemVersionMajor'
    )
  ) as os_version_major, 
  maxSimpleState(derived_tstamp) AS last_timestamp, 
  minSimpleState(derived_tstamp) AS first_timestamp 
FROM 
  %s.snowplow_events 
WHERE 
  user_id_type IN ('cookie', 'identify') 
GROUP BY 
  domain_sessionid;`, projectId, projectId, projectId)

		_, err = tx.ExecContext(ctx, sessionsViews)
		if err != nil {
			return err
		}
	}

	return nil
}

func downMigrateExistingProjectsWithSessions(ctx context.Context, tx *sql.Tx) error {
	// no-op. Do not delete existing sessions tables once migrated.
	return nil
}
