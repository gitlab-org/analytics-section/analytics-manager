CREATE TABLE IF NOT EXISTS default.active_apps
(
    app_id     String,
    project_id String,
    created_at DateTime64(3, 'GMT')
) ENGINE = MergeTree()
      PRIMARY KEY (project_id, created_at)