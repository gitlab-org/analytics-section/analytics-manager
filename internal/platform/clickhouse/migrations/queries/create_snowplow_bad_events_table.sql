CREATE TABLE IF NOT EXISTS default.snowplow_bad_events
(
    schema String,
    data String,
    timestamp DateTime64(3, 'GMT')
) engine = MergeTree
      ORDER BY timestamp
