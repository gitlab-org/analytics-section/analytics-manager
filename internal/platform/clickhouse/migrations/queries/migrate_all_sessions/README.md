This folder contains SQL queries as part of a plan to repopulate every project's `sessions` table.

For each project, the following steps will occur.

* Rename `snowplow_events` to `snowplow_events_backup`.
* Create a new table, `snowplow_events` with an identical schema.
* Delete all existing sessions.
* Reinsert the contents of `snowplow_events_backup` in to `snowplow_events`.
    * This will cause the repopulation of the `sessions` table and materialized view.

They should be executed as part of a migration, in order and **event ingestion must be disabled before the migration is run.**

## Prerequisites

- Event ingestion must be disabled.
- ClickHouse Cloud RAM should be increased to around 200GB until the migration is complete.