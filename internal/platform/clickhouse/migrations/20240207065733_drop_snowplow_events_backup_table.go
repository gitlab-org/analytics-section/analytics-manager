package migrations

import (
	"context"
	"database/sql"
	"fmt"
	"github.com/pressly/goose/v3"
)

func init() {
	goose.AddMigrationContext(upDropSnowplowEventsBackupTable, downDropSnowplowEventsBackupTable)
}

func upDropSnowplowEventsBackupTable(ctx context.Context, tx *sql.Tx) error {
	// This code is executed when the migration is applied.
	var projectIds []string

	hasActiveAppsTable, err := activeAppsTableExists(ctx, tx)
	switch {
	case err != nil:
		return err
	case !hasActiveAppsTable:
		return nil
	}

	rows, err := tx.QueryContext(ctx, "SELECT Distinct project_id from default.active_apps")
	if err != nil {
		return err
	}
	defer rows.Close()

	for rows.Next() {
		var projectId string
		if err := rows.Scan(&projectId); err != nil {
			return err
		}
		projectIds = append(projectIds, projectId)
	}

	for _, projectId := range projectIds {
		_, err = tx.ExecContext(ctx, fmt.Sprintf("DROP TABLE IF EXISTS %s.snowplow_events_backup", projectId))
		if err != nil {
			return err
		}
	}

	return nil
}

func downDropSnowplowEventsBackupTable(ctx context.Context, tx *sql.Tx) error {
	// This code is executed when the migration is rolled back.
	return nil
}
