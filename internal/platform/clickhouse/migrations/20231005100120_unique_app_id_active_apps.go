package migrations

import (
	"context"
	"database/sql"

	"github.com/pressly/goose/v3"
)

func init() {
	goose.AddMigrationContext(upUniqueAppIdActiveApps, downUniqueAppIdActiveApps)
}

func upUniqueAppIdActiveApps(ctx context.Context, tx *sql.Tx) error {

	hasActiveAppsTable, err := activeAppsTableExists(ctx, tx)
	switch {
	case err != nil:
		return err
	case !hasActiveAppsTable:
		return nil
	}

	/**
	The query performs the following:
	1. Identifies projects with multiple app_ids.
	2. Determines the earliest creation timestamp for each project.
	3. For projects with multiple app_ids, deletes records except the one with the earliest creation timestamp.
	*/
	_, err = tx.ExecContext(ctx, `
		ALTER TABLE default.active_apps 
		DELETE WHERE (project_id, created_at) NOT IN (
			SELECT project_id, MIN(created_at)
			FROM default.active_apps 
			GROUP BY project_id
		)
		AND project_id IN (
			SELECT project_id 
			FROM default.active_apps 
			GROUP BY project_id
			HAVING uniqExact(app_id) > 1
		);
    `)
	if err != nil {
		return err
	}

	return nil
}

func downUniqueAppIdActiveApps(ctx context.Context, tx *sql.Tx) error {
	// This code is executed when the migration is rolled back.
	return nil
}
