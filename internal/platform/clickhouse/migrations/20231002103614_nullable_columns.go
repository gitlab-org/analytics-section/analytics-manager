package migrations

import (
	"context"
	"database/sql"
	"fmt"
	"strings"

	"github.com/pressly/goose/v3"
)

func init() {
	goose.AddMigrationContext(upNullableColumns, downNullableColumns)
}

func upNullableColumns(ctx context.Context, tx *sql.Tx) error {

	var tableExists int
	err := tx.QueryRowContext(ctx, "EXISTS TABLE default.active_apps").Scan(&tableExists)
	if err != nil {
		return err
	}

	// If active_apps doesn't exist, that means `/setup-clickhouse` is not executed yet, so we do not need migration
	if tableExists == 0 {
		return nil
	}

	err = alterTableColumns(ctx, tx, "default.snowplow_queue")
	if err != nil {
		return err
	}

	var projectIds []string

	rows, err := tx.QueryContext(ctx, "SELECT Distinct project_id from default.active_apps")
	if err != nil {
		return err
	}
	defer rows.Close()

	for rows.Next() {
		var projectId string
		if err := rows.Scan(&projectId); err != nil {
			return err
		}
		projectIds = append(projectIds, projectId)
	}

	for _, projectId := range projectIds {
		tableName := fmt.Sprintf("%s.snowplow_events", projectId)
		err = alterTableColumns(ctx, tx, tableName)
		if err != nil {
			return err
		}
	}

	return nil

}

func downNullableColumns(ctx context.Context, tx *sql.Tx) error {
	// This code is executed when the migration is rolled back.
	return nil
}

func alterTableColumns(ctx context.Context, tx *sql.Tx, tableName string) error {
	modifyColumnsFragment := `
	MODIFY COLUMN etl_tstamp Nullable(DateTime64(3, 'GMT')),
	MODIFY COLUMN dvce_created_tstamp Nullable(DateTime64(3, 'GMT')),
	MODIFY COLUMN txn_id Nullable(String),
	MODIFY COLUMN name_tracker Nullable(String),
	MODIFY COLUMN user_id Nullable(String),
	MODIFY COLUMN user_ipaddress Nullable(String),
	MODIFY COLUMN user_fingerprint Nullable(String),
	MODIFY COLUMN domain_userid Nullable(String),
	MODIFY COLUMN domain_sessionidx Nullable(String),
	MODIFY COLUMN network_userid Nullable(String),
	MODIFY COLUMN geo_country Nullable(String),
	MODIFY COLUMN geo_region Nullable(String),
	MODIFY COLUMN geo_city Nullable(String),
	MODIFY COLUMN geo_zipcode Nullable(String),
	MODIFY COLUMN geo_latitude Nullable(String),
	MODIFY COLUMN geo_longitude Nullable(String),
	MODIFY COLUMN geo_region_name Nullable(String),
	MODIFY COLUMN ip_isp Nullable(String),
	MODIFY COLUMN ip_organization Nullable(String),
	MODIFY COLUMN ip_domain Nullable(String),
	MODIFY COLUMN ip_netspeed Nullable(String),
	MODIFY COLUMN page_url Nullable(String),
	MODIFY COLUMN page_title Nullable(String),
	MODIFY COLUMN page_referrer Nullable(String),
	MODIFY COLUMN page_urlscheme Nullable(String),
	MODIFY COLUMN page_urlhost Nullable(String),
	MODIFY COLUMN page_urlport Nullable(String),
	MODIFY COLUMN page_urlpath Nullable(String),
	MODIFY COLUMN page_urlquery Nullable(String),
	MODIFY COLUMN page_urlfragment Nullable(String),
	MODIFY COLUMN refr_urlscheme Nullable(String),
	MODIFY COLUMN refr_urlhost Nullable(String),
	MODIFY COLUMN refr_urlport Nullable(String),
	MODIFY COLUMN refr_urlpath Nullable(String),
	MODIFY COLUMN refr_urlquery Nullable(String),
	MODIFY COLUMN refr_urlfragment Nullable(String),
	MODIFY COLUMN refr_medium Nullable(String),
	MODIFY COLUMN refr_source Nullable(String),
	MODIFY COLUMN refr_term Nullable(String),
	MODIFY COLUMN mkt_medium Nullable(String),
	MODIFY COLUMN mkt_source Nullable(String),
	MODIFY COLUMN mkt_term Nullable(String),
	MODIFY COLUMN mkt_content Nullable(String),
	MODIFY COLUMN mkt_campaign Nullable(String),
	MODIFY COLUMN pp_xoffset_min Nullable(String),
	MODIFY COLUMN pp_xoffset_max Nullable(String),
	MODIFY COLUMN pp_yoffset_min Nullable(String),
	MODIFY COLUMN pp_yoffset_max Nullable(String),
	MODIFY COLUMN useragent Nullable(String),
	MODIFY COLUMN br_name Nullable(String),
	MODIFY COLUMN br_family Nullable(String),
	MODIFY COLUMN br_version Nullable(String),
	MODIFY COLUMN br_type Nullable(String),
	MODIFY COLUMN br_renderengine Nullable(String),
	MODIFY COLUMN br_lang Nullable(String),
	MODIFY COLUMN br_features_pdf Nullable(String),
	MODIFY COLUMN br_cookies Nullable(String),
	MODIFY COLUMN br_colordepth Nullable(Int16),
	MODIFY COLUMN br_viewwidth Nullable(Int16),
	MODIFY COLUMN br_viewheight Nullable(Int16),
	MODIFY COLUMN os_name Nullable(String),
	MODIFY COLUMN os_family Nullable(String),
	MODIFY COLUMN os_manufacturer Nullable(String),
	MODIFY COLUMN os_timezone Nullable(String),
	MODIFY COLUMN dvce_type Nullable(String),
	MODIFY COLUMN dvce_ismobile Nullable(String),
	MODIFY COLUMN dvce_screenwidth Nullable(Int16),
	MODIFY COLUMN dvce_screenheight Nullable(Int16),
	MODIFY COLUMN doc_charset Nullable(String),
	MODIFY COLUMN doc_width Nullable(String),
	MODIFY COLUMN doc_height Nullable(String),
	MODIFY COLUMN geo_timezone Nullable(String),
	MODIFY COLUMN mkt_clickid Nullable(String),
	MODIFY COLUMN mkt_network Nullable(String),
	MODIFY COLUMN etl_tags Nullable(String),
	MODIFY COLUMN dvce_sent_tstamp Nullable(DateTime64(3, 'GMT')),
	MODIFY COLUMN refr_domain_userid Nullable(String),
	MODIFY COLUMN refr_device_tstamp Nullable(String),
	MODIFY COLUMN derived_contexts Nullable(String),
	MODIFY COLUMN domain_sessionid Nullable(String),
	MODIFY COLUMN event_vendor Nullable(String),
	MODIFY COLUMN event_name Nullable(String),
	MODIFY COLUMN event_format Nullable(String),
	MODIFY COLUMN event_version Nullable(String),
	MODIFY COLUMN event_fingerprint Nullable(String),
	MODIFY COLUMN true_tstamp Nullable(String),
	MODIFY COLUMN contexts String,
	MODIFY COLUMN unstruct_event String;
	`

	modifyEventsFragment := `MODIFY COLUMN custom_event_name Nullable(String),
	MODIFY COLUMN custom_event_props Nullable(String),
	MODIFY COLUMN custom_user_props Nullable(String);`

	modifyQueueFragment := `MODIFY COLUMN se_category Nullable(String),
	MODIFY COLUMN se_action Nullable(String),
	MODIFY COLUMN se_label Nullable(String),
	MODIFY COLUMN se_property Nullable(String),
	MODIFY COLUMN se_value Nullable(Decimal32(2)),
	MODIFY COLUMN tr_orderid Nullable(String),
	MODIFY COLUMN tr_affiliation Nullable(String),
	MODIFY COLUMN tr_total Nullable(Decimal32(2)),
	MODIFY COLUMN tr_tax Nullable(Decimal32(2)),
	MODIFY COLUMN tr_shipping Nullable(Decimal32(2)),
	MODIFY COLUMN tr_city Nullable(String),
	MODIFY COLUMN tr_state Nullable(String),
	MODIFY COLUMN tr_country Nullable(String),
	MODIFY COLUMN ti_orderid Nullable(String),
	MODIFY COLUMN ti_sku Nullable(String),
	MODIFY COLUMN ti_name Nullable(String),
	MODIFY COLUMN ti_category Nullable(String),
	MODIFY COLUMN ti_price Nullable(Decimal32(2)),
	MODIFY COLUMN ti_quantity Nullable(Int16),
	MODIFY COLUMN br_features_flash Nullable(String),
	MODIFY COLUMN br_features_java Nullable(String),
	MODIFY COLUMN br_features_director Nullable(String),
	MODIFY COLUMN br_features_quicktime Nullable(String),
	MODIFY COLUMN br_features_realplayer Nullable(String),
	MODIFY COLUMN br_features_windowsmedia Nullable(String),
	MODIFY COLUMN br_features_gears Nullable(String),
	MODIFY COLUMN br_features_silverlight Nullable(String),
	MODIFY COLUMN tr_currency Nullable(String),
	MODIFY COLUMN tr_total_base Nullable(Decimal32(2)),
	MODIFY COLUMN tr_tax_base Nullable(Decimal32(2)),
	MODIFY COLUMN tr_shipping_base Nullable(Decimal32(2)),
	MODIFY COLUMN ti_currency Nullable(String),
	MODIFY COLUMN ti_price_base Nullable(Decimal32(2)),
	MODIFY COLUMN base_currency Nullable(String);
	`

	query := fmt.Sprintf("ALTER TABLE %s %s", tableName, modifyColumnsFragment)
	_, err := tx.ExecContext(ctx, query)
	if err != nil {
		return err
	}

	if strings.Contains(tableName, "snowplow_events") {
		query = fmt.Sprintf("ALTER TABLE %s %s", tableName, modifyEventsFragment)
		_, err = tx.ExecContext(ctx, query)
		if err != nil {
			return err
		}
	}

	if tableName == "default.snowplow_queue" {
		query = fmt.Sprintf("ALTER TABLE %s %s", tableName, modifyQueueFragment)
		_, err = tx.ExecContext(ctx, query)
		if err != nil {
			return err
		}
	}

	return err
}
