package migrations

import (
	"context"
	"database/sql"

	"github.com/pressly/goose/v3"
)

func init() {
	goose.AddMigrationContext(upSetupClickhouseEntities, downSetupClickhouseEntities)
}

func upSetupClickhouseEntities(_ context.Context, tx *sql.Tx) error {
	paths := []string{
		"queries/create_snowplow_queue_table.sql",
		"queries/create_enriched_events_table.sql",
		"queries/create_enriched_events_view.sql",
		"queries/create_active_apps_table.sql",
	}

	for _, path := range paths {
		query, err := f.ReadFile(path)
		if err != nil {
			return err
		}

		if _, err := tx.Exec(string(query)); err != nil {
			return err
		}

	}

	_, err := tx.Exec(`
      CREATE FUNCTION IF NOT EXISTS extract_schema_data as 
        (contexts, schema_name) -> JSONExtractString(arrayFirst(ctx -> visitParamExtractString(ctx, 'schema') like schema_name, JSONExtractArrayRaw(contexts, 'data')), 'data');
    `,
	)
	if err != nil {
		return err
	}

	paths = []string{
		"queries/create_snowplow_bad_events_queue_table.sql",
		"queries/create_snowplow_bad_events_table.sql",
		"queries/create_snowplow_bad_events_view.sql",
	}

	for _, path := range paths {
		query, err := f.ReadFile(path)
		if err != nil {
			return err
		}

		if _, err := tx.Exec(string(query)); err != nil {
			return err
		}
	}

	return nil
}

func downSetupClickhouseEntities(_ context.Context, _ *sql.Tx) error {
	return nil
}
