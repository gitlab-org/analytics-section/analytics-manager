package migrations

import (
	"context"
	"database/sql"
	"fmt"
	"github.com/pressly/goose/v3"
	"gitlab.com/gitlab-org/analytics-section/analytics-manager/internal/platform/utils"
)

func init() {
	goose.AddMigrationContext(upMigrateExistingProjectsWithFunnelSchemas, downMigrateExistingProjectsWithFunnelSchemas)
}

func upMigrateExistingProjectsWithFunnelSchemas(ctx context.Context, tx *sql.Tx) error {
	var projectIDs []string

	hasActiveAppsTable, err := activeAppsTableExists(ctx, tx)
	switch {
	case err != nil:
		return err
	case !hasActiveAppsTable:
		return nil
	}

	rows, err := tx.QueryContext(ctx, "SELECT Distinct project_id from default.active_apps")
	if err != nil {
		return err
	}
	defer rows.Close()

	for rows.Next() {
		var projectID string
		if err := rows.Scan(&projectID); err != nil {
			return err
		}
		projectIDs = append(projectIDs, projectID)
	}

	for _, projectID := range projectIDs {
		createFunnelSchemasTableQuery := fmt.Sprintf(`
			CREATE TABLE IF NOT EXISTS %s.funnel_schemas
			(
				name String,
				contents String,
				created_at DateTime64(3, 'GMT'),
				updated_at DateTime64(3, 'GMT')
			)
				ENGINE = MergeTree
				PRIMARY KEY (name, created_at);
		`, utils.SanitizeString(projectID))

		_, err = tx.ExecContext(ctx, createFunnelSchemasTableQuery)
		if err != nil {
			return err
		}
	}

	return nil
}

func downMigrateExistingProjectsWithFunnelSchemas(ctx context.Context, tx *sql.Tx) error {
	// no-op. Do not delete existing funnel_schemas tables once migrated.
	return nil
}
