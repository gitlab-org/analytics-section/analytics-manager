package migrations

import (
	"context"
	"database/sql"
	"embed"
	"fmt"

	"github.com/pressly/goose/v3"
)

//go:embed queries/* queries/migrate_all_sessions/*
var f embed.FS

func init() {
	goose.AddMigrationContext(upAddUserIdType, downAddUserIdType)
}

func upAddUserIdType(ctx context.Context, tx *sql.Tx) error {
	var projectIds []string

	hasActiveAppsTable, err := activeAppsTableExists(ctx, tx)
	switch {
	case err != nil:
		return err
	case !hasActiveAppsTable:
		return nil
	}

	rows, err := tx.QueryContext(ctx, "SELECT Distinct project_id from default.active_apps")
	if err != nil {
		return err
	}
	defer rows.Close()

	for rows.Next() {
		var projectId string
		if err := rows.Scan(&projectId); err != nil {
			return err
		}
		projectIds = append(projectIds, projectId)
	}

	for _, projectId := range projectIds {
		altSnowplowEvents := fmt.Sprintf("ALTER TABLE %s.snowplow_events ADD COLUMN IF NOT EXISTS user_id_type  Enum('anonymous', 'cookie', 'identify') AFTER custom_user_props", projectId)
		_, err := tx.ExecContext(ctx, altSnowplowEvents)
		if err != nil {
			return err
		}

		updateUserId := fmt.Sprintf("ALTER TABLE %s.snowplow_events UPDATE user_id=multiIf(user_id != '', user_id, domain_userid != '', domain_userid, event_id) WHERE user_id=''", projectId)
		_, err = tx.ExecContext(ctx, updateUserId)
		if err != nil {
			return err
		}

		updateUserIdType := fmt.Sprintf("ALTER TABLE %s.snowplow_events UPDATE user_id_type=multiIf(user_id = event_id, 'anonymous', user_id = domain_userid, 'cookie', 'identify') WHERE user_id_type='anonymous'", projectId)
		_, err = tx.ExecContext(ctx, updateUserIdType)
		if err != nil {
			return err
		}

		_, err = tx.ExecContext(ctx, fmt.Sprintf("DROP TABLE IF EXISTS %s.snowplow_consumer", projectId))
		if err != nil {
			return err
		}

		var appId string
		err = tx.QueryRow("SELECT app_id FROM default.active_apps WHERE project_id = ? ORDER BY created_at DESC LIMIT 1", projectId).Scan(&appId)
		if err != nil {
			return err
		}

		_, err = tx.Exec(fmt.Sprintf(createProjectView, projectId, projectId, appId))
		if err != nil {
			return err
		}
	}

	return nil
}

func downAddUserIdType(ctx context.Context, tx *sql.Tx) error {
	// This code is executed when the migration is rolled back.
	return nil
}

var createProjectView = `CREATE MATERIALIZED VIEW IF NOT EXISTS %s.snowplow_consumer
    TO %s.snowplow_events
AS
SELECT app_id,
       platform,
       etl_tstamp,
       collector_tstamp,
       dvce_created_tstamp,
       event,
       event_id,
       txn_id,
       name_tracker,
       v_tracker,
       v_collector,
       v_etl,
       multiIf(user_id != '', user_id, domain_userid != '', domain_userid, event_id) as user_id,
       user_ipaddress,
       user_fingerprint,
       domain_userid,
       domain_sessionidx,
       network_userid,
       geo_country,
       geo_region,
       geo_city,
       geo_zipcode,
       geo_latitude,
       geo_longitude,
       geo_region_name,
       ip_isp,
       ip_organization,
       ip_domain,
       ip_netspeed,
       page_url,
       page_title,
       page_referrer,
       page_urlscheme,
       page_urlhost,
       page_urlport,
       page_urlpath,
       page_urlquery,
       page_urlfragment,
       refr_urlscheme,
       refr_urlhost,
       refr_urlport,
       refr_urlpath,
       refr_urlquery,
       refr_urlfragment,
       refr_medium,
       refr_source,
       refr_term,
       mkt_medium,
       mkt_source,
       mkt_term,
       mkt_content,
       mkt_campaign,
       contexts,
       unstruct_event,
       pp_xoffset_min,
       pp_xoffset_max,
       pp_yoffset_min,
       pp_yoffset_max,
       useragent,
       br_name,
       br_family,
       br_version,
       br_type,
       br_renderengine,
       br_lang,
       br_features_pdf,
       br_cookies,
       br_colordepth,
       br_viewwidth,
       br_viewheight,
       os_name,
       os_family,
       os_manufacturer,
       os_timezone,
       dvce_type,
       dvce_ismobile,
       dvce_screenwidth,
       dvce_screenheight,
       doc_charset,
       doc_width,
       doc_height,
       geo_timezone,
       mkt_clickid,
       mkt_network,
       etl_tags,
       dvce_sent_tstamp,
       refr_domain_userid,
       refr_device_tstamp,
       derived_contexts,
       domain_sessionid,
       derived_tstamp,
       event_vendor,
       event_name,
       event_format,
       event_version,
       event_fingerprint,
       true_tstamp,
       if(event_name = 'custom_event', visitParamExtractString(unstruct_event, 'name'), '') as custom_event_name,
       if(event_name = 'custom_event', JSONExtractString(unstruct_event, 'data', 'data', 'props'),
          '')                                                                               as custom_event_props,
       extract_schema_data(contexts, '%%user_context%%')                                      as custom_user_props,
       multiIf(user_id = event_id, 'anonymous', user_id = domain_userid, 'cookie', 'identify') as user_id_type

FROM default.snowplow_queue
WHERE app_id = '%s'
`

func activeAppsTableExists(ctx context.Context, tx *sql.Tx) (bool, error) {
	var exists int
	err := tx.QueryRowContext(ctx, "EXISTS TABLE default.active_apps").Scan(&exists)
	if err != nil {
		return false, err
	}

	return exists == 1, nil
}
