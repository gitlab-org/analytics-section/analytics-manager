package clickhouse

import (
	"database/sql"
	"embed"
	"errors"
	"fmt"
	"log"
	"strings"
	"time"

	"gitlab.com/gitlab-org/analytics-section/analytics-manager/internal/platform/utils"
)

//go:embed queries/*
var f embed.FS

const (
	driverName    = "clickhouse"
	migrationsDir = "migrations"
)

type Clickhouse struct {
	db *sql.DB
}

// ProjectUsageByMonth returns the number of rows used by a particular projects database
func (c *Clickhouse) ProjectUsageByMonth(projectId string, year int, month int) (int, error) {
	var result int
	row := c.db.QueryRow(fmt.Sprintf(`SELECT COUNT(*) AS count FROM %s.snowplow_events WHERE derived_tstamp BETWEEN makeDate(%d, %d, 01) AND toStartOfMonth(makeDate(%d, %d, 01) + interval 1 month) - interval 1 day`, utils.SanitizeString(projectId), year, month, year, month))
	err := row.Scan(&result)

	if err != nil {
		log.Print(fmt.Errorf("could not query row for project usage: %w", err))
	}

	return result, err
}

func ConnectToDB(clickhouseDsn string) (*Clickhouse, error) {
	conn, err := sql.Open(driverName, clickhouseDsn)
	if err != nil {
		return nil, err
	}

	err = conn.Ping()
	if err != nil {
		return nil, err
	}

	return &Clickhouse{db: conn}, nil
}

// CreateDatabase creates a new database if it doesn't exist and returns an error if it fails
func (c *Clickhouse) CreateDatabase(databaseName string) error {
	_, err := c.db.Exec("CREATE DATABASE IF NOT EXISTS " + utils.SanitizeString(databaseName))
	return err
}

// CreateProjectSnowplowEventsTable creates a new snowplow_events tables for a new project and returns an error if it fails
func (c *Clickhouse) CreateProjectSnowplowEventsTable(projectId string, isCluster bool) error {
	var err error

	if isCluster {
		query, err := f.ReadFile("queries/create_replicated_snowplow_events_table.sql")
		if err != nil {
			log.Fatal(err)
		}

		_, err = c.db.Exec(string(query),
			sql.Named("database_name", projectId),
		)
		if err != nil {
			log.Fatal(err)
		}

		query, err = f.ReadFile("queries/create_distributed_snowplow_events_table.sql")
		if err != nil {
			log.Fatal(err)
		}

		// CH Cluster doesn't support named param in AS, need to substitute it explicitly
		queryWithDBName := strings.ReplaceAll(string(query), "{database_name:Identifier}", utils.SanitizeString(projectId))
		_, err = c.db.Exec(queryWithDBName)
		if err != nil {
			log.Fatal(err)
		}

	} else {
		query, err := f.ReadFile("queries/create_project_snowplow_events_table.sql")
		if err != nil {
			log.Fatal(err)
		}

		_, err = c.db.Exec(string(query),
			sql.Named("database_name", projectId),
		)
	}
	return err
}

// GetAppIdByProjectId returns corresponding appId for the projectId
func (c *Clickhouse) GetAppIdByProjectId(projectId string) (*string, error) {
	var appId string
	err := c.db.QueryRow("SELECT app_id FROM default.active_apps WHERE project_id = $1", projectId).Scan(&appId)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return nil, nil
		}
		return nil, err
	}
	return &appId, nil
}

// InsertIntoActiveApps inserts a new app into the active_apps_table and returns an error if it fails
func (c *Clickhouse) InsertIntoActiveApps(appId string, projectId string) error {
	_, err := c.db.Exec("INSERT INTO default.active_apps (app_id, project_id, created_at) VALUES ($1, $2, $3)", appId, projectId, time.Now())
	return err
}

// CreateProjectView creates a new project_view and returns an error if it fails
func (c *Clickhouse) CreateProjectView(appId string, projectId string) error {
	newProjectViewQuery, err := f.ReadFile("queries/create_project_view.sql")
	if err != nil {
		log.Fatal(err)
	}

	newProjectViewQueryInterpolated := strings.ReplaceAll(string(newProjectViewQuery), "{database_name:Identifier}", projectId)

	_, err = c.db.Exec(newProjectViewQueryInterpolated,
		sql.Named("app_id", appId),
	)

	return err
}

// CreateSessionsTable creates a new mv fore sessions and returns an error if it fails
func (c *Clickhouse) CreateSessionsTable(projectId string) error {
	newSessionsTableQuery, err := f.ReadFile("queries/create_sessions_table.sql")
	if err != nil {
		log.Fatal(err)
	}

	newSessionsTableQueryInterpolated := strings.ReplaceAll(string(newSessionsTableQuery), "{database_name:Identifier}", projectId)

	_, err = c.db.Exec(newSessionsTableQueryInterpolated)

	return err
}

// CreatePartitionedSessionsTable creates a new table with custom partitioning key for sessions and returns an error if it fails
func (c *Clickhouse) CreatePartitionedSessionsTable(projectID string) error {
	newPartitionedSessionsTableQuery, err := f.ReadFile("queries/create_partitioned_sessions_table.sql")
	if err != nil {
		log.Fatal(err)
	}

	newPartitionedSessionsTableQueryInterpolated := strings.ReplaceAll(string(newPartitionedSessionsTableQuery), "{database_name:Identifier}", projectID)

	_, err = c.db.Exec(newPartitionedSessionsTableQueryInterpolated)

	return err
}

// CreatePartitionedSessionsView creates a new mv for sessions partition table and returns an error if it fails
func (c *Clickhouse) CreatePartitionedSessionsView(projectID string) error {
	newPartitionedSessionsViewQuery, err := f.ReadFile("queries/create_sessions_partition_view.sql")
	if err != nil {
		log.Fatal(err)
	}

	newPartitionedSessionsTableViewInterpolated := strings.ReplaceAll(string(newPartitionedSessionsViewQuery), "{database_name:Identifier}", projectID)

	_, err = c.db.Exec(newPartitionedSessionsTableViewInterpolated)

	return err
}

// CreateSessionsView creates a new mv fore sessions and returns an error if it fails
func (c *Clickhouse) CreateSessionsView(projectId string) error {
	newSessionsViewQuery, err := f.ReadFile("queries/create_sessions_view.sql")
	if err != nil {
		log.Fatal(err)
	}

	newSessionsViewQueryInterpolated := strings.ReplaceAll(string(newSessionsViewQuery), "{database_name:Identifier}", projectId)

	_, err = c.db.Exec(newSessionsViewQueryInterpolated)

	return err
}

// DropProjectDatabases deletes all project databases (all projects with records in `active_apps` table)
func (c *Clickhouse) DropProjectDatabases() ([]string, error) {
	dbNames, err := c.GetAllProjectDBNames()
	if err != nil {
		return nil, err
	}
	for _, dbName := range dbNames {
		_, err := c.db.Exec("DROP DATABASE IF EXISTS " + utils.SanitizeString(dbName))
		if err != nil {
			log.Print(err)
			return nil, err
		}
	}
	return dbNames, nil
}

// CleanDefaultDb deletes all tables and views from the default database
func (c *Clickhouse) CleanDefaultDb() ([]string, error) {
	tableNames, err := c.getAllEntitiesFromDB("TABLES", "default")
	if err != nil {
		return nil, err
	}
	for _, table := range tableNames {
		err := c.deleteEntityFromDB("table", "default."+table)
		if err != nil {
			return nil, err
		}
	}

	return tableNames, nil
}

// GetAllProjectDBNames returns all project database names
func (c *Clickhouse) GetAllProjectDBNames() ([]string, error) {
	var dbNames []string
	var exists int
	err := c.db.QueryRow("EXISTS TABLE default.active_apps").Scan(&exists)
	if err != nil {
		return nil, err
	}

	if exists == 0 {
		return dbNames, nil
	}

	rows, err := c.db.Query("SELECT DISTINCT project_id FROM default.active_apps")
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	for rows.Next() {
		var entity string
		if err := rows.Scan(&entity); err != nil {
			return nil, err
		}
		dbNames = append(dbNames, entity)
	}
	if err = rows.Err(); err != nil {
		return nil, err
	}
	return dbNames, nil
}

// GetPendingMigrations Get the amount of pending migrations
func (c *Clickhouse) GetPendingMigrations() (int, error) {
	var count int
	err := c.db.QueryRow("SELECT COUNT(*) FROM default.goose_db_version WHERE is_applied = 0").Scan(&count)
	if err != nil {
		return 0, err
	}
	return count, nil
}

// CreateFunnelSchemasTable creates a new funnel_schemas table and returns an error if it fails
func (c *Clickhouse) CreateFunnelSchemasTable(projectId string) error {
	newFunnelSchemasTableQuery, err := f.ReadFile("queries/create_funnel_schemas_table.sql")
	if err != nil {
		log.Fatal(err)
	}

	newFunnelSchemasTableQueryInterpolated := strings.ReplaceAll(string(newFunnelSchemasTableQuery), "{database_name:Identifier}", projectId)

	_, err = c.db.Exec(newFunnelSchemasTableQueryInterpolated)

	return err
}

func (c *Clickhouse) InsertFunnelSchema(projectID string, name string, contents string) error {
	insertFunnelSchemaQuery, err := f.ReadFile("queries/insert_into_funnel_schemas.sql")
	if err != nil {
		return err
	}

	insertFunnelSchemaQueryInterpolated := strings.ReplaceAll(string(insertFunnelSchemaQuery), "{database_name:Identifier}", projectID)
	now := time.Now()

	_, err = c.db.Exec(insertFunnelSchemaQueryInterpolated, name, contents, now, now)

	return err
}

func (c *Clickhouse) DeleteFunnelSchema(projectID string, name string) error {
	deleteFunnelSchemaQuery, err := f.ReadFile("queries/delete_funnel_schema.sql")
	if err != nil {
		return err
	}

	deleteFunnelSchemaQueryInterpolated := strings.ReplaceAll(string(deleteFunnelSchemaQuery), "{database_name:Identifier}", projectID)

	_, err = c.db.Exec(deleteFunnelSchemaQueryInterpolated, name)
	return err
}

func (c *Clickhouse) getAllEntitiesFromDB(entityName string, database string) ([]string, error) {
	var entities []string
	rows, err := c.db.Query(fmt.Sprintf("SHOW %s FROM %s", entityName, database))
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var entity string
		if err := rows.Scan(&entity); err != nil {
			return nil, err
		}
		entities = append(entities, entity)
	}
	if err = rows.Err(); err != nil {
		return nil, err
	}

	return entities, nil
}

func (c *Clickhouse) deleteEntityFromDB(entityName string, table string) error {
	_, err := c.db.Exec(fmt.Sprintf("DROP %s IF EXISTS %s", entityName, table))
	return err
}
