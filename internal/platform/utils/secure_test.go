package utils

import (
	"testing"
)

func TestSanitizeStringCorrectInput(t *testing.T) {
	str := SanitizeString("test")
	if str != "test" {
		t.Fatal("Sanitized string is not the same as input")
	}
}

func TestSanitizeStringBadInput(t *testing.T) {
	str := SanitizeString("test; DROP DATABASE --A'")
	if str != "test DROP DATABASE A" {
		t.Fatal("Sanitized string is not the same as input")
	}
}
