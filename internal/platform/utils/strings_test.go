package utils

import "testing"

func TestStartsWithLetter(t *testing.T) {
	tests := []struct {
		name string
		s    string
		want bool
	}{
		{
			name: "empty string",
			s:    "",
			want: false,
		},
		{
			name: "string with number",
			s:    "1",
			want: false,
		},
		{
			name: "string with letter",
			s:    "a",
			want: true,
		},
		{
			name: "string with letter and number",
			s:    "a1",
			want: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := StartsWithLetter(tt.s); got != tt.want {
				t.Errorf("StartsWithLetter() = %v, want %v", got, tt.want)
			}
		})
	}
}
