package utils

import (
	"strings"
)

// SanitizeString removes characters that could be used for SQL injection
func SanitizeString(str string) string {
	str = strings.ReplaceAll(str, ";", "")
	str = strings.ReplaceAll(str, "--", "")
	str = strings.ReplaceAll(str, "'", "")
	str = strings.ReplaceAll(str, "\"", "")

	return str
}
