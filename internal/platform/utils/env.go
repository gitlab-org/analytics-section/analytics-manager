// Package utils: Utility functions for the project
package utils

import (
	"log"
	"os"
)

// GetEnv returns the value of the environment variable with the given key or fails if value is not set.
func GetEnv(key string) string {
	value, ok := os.LookupEnv(key)
	if !ok {
		log.Fatalf("Environment variable %s is not set", key)
	}
	return value
}

// GetEnvFlag returns the value of the environment variable with the given key or false if value is not set.
func GetEnvFlag(key string) bool {
	value, ok := os.LookupEnv(key)
	if !ok {
		return false
	}
	return value == "true" || value == "1"
}
