FROM golang:alpine as builder
WORKDIR /workspace
COPY . /workspace/
RUN go mod download
RUN GOOS=linux GOARCH=amd64 CGO_ENABLED=0 go build -o servid ./cmd/servid
RUN GOOS=linux GOARCH=amd64 CGO_ENABLED=0 go build -o migrate ./cmd/migrate

FROM golang:alpine AS runner
WORKDIR /workspace
COPY --from=builder /workspace/servid .
COPY --from=builder /workspace/migrate .
EXPOSE 4567
ENV GIN_MODE=release
ENTRYPOINT ["./servid"]
