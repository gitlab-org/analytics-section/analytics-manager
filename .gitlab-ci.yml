image: golang:latest
include:
  - template: Security/SAST.gitlab-ci.yml
  - template: Security/Dependency-Scanning.gitlab-ci.yml
  - template: Security/Container-Scanning.gitlab-ci.yml
  - component: ${CI_SERVER_FQDN}/gitlab-org/components/danger-review/danger-review@1.2.0

variables:
  OUTPUT_NAME: __bin__/$CI_PROJECT_NAME


container_scanning:
  variables:
    CS_IMAGE: $CI_REGISTRY_IMAGE:$CI_COMMIT_TAG
  rules:
    - if: $CI_COMMIT_TAG

stages:
  - lint
  - build
  - test
  - e2e

lint:
  image: registry.gitlab.com/gitlab-org/gitlab-build-images:golangci-lint-alpine
  stage: lint
  script:
    # Use default .golangci.yml file from the image if one is not present in the project root.
    - '[ -e .golangci.yml ] || cp /golangci/.golangci.yml .'
    # Write the code coverage report to gl-code-quality-report.json
    # and print linting issues to stdout in the format: path/to/file:line description
    # remove `--issues-exit-code 0` or set to non-zero to fail the job if linting issues are detected
    - golangci-lint run --issues-exit-code 0 --print-issued-lines=false --out-format code-climate:gl-code-quality-report.json,line-number
  artifacts:
    reports:
      codequality: gl-code-quality-report.json
    paths:
      - gl-code-quality-report.json
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH && $CI_PIPELINE_SOURCE == "push"'

test:
  stage: test
  script:
    - go fmt $(go list ./... | grep -v /vendor/)
    - go vet $(go list ./... | grep -v /vendor/)
    - go install gotest.tools/gotestsum@latest
    - gotestsum --junitfile report.xml --format testname -- --short -json ./...
  artifacts:
    when: always
    reports:
      junit: report.xml
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH && $CI_PIPELINE_SOURCE == "push"'

test_build_container:
  image: docker:20.10.16
  services:
    - docker:20.10.16-dind
  stage: e2e
  script:
    - docker build .
  needs:
    - job: test
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH && $CI_PIPELINE_SOURCE == "push"'

e2e_test_migrations:
  stage: e2e
  script:
    - go fmt $(go list ./... | grep -v /vendor/)
    - go vet $(go list ./... | grep -v /vendor/)
    - go install gotest.tools/gotestsum@latest
    - gotestsum --junitfile report.xml --format testname -- -json ./cmd/tests/e2e/...
  needs:
    - job: test
  resource_group: ch_cloud
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH && $CI_PIPELINE_SOURCE == "push"'

prepare_job:
  stage: build
  image: docker:20.10.16
  services:
    - docker:20.10.16-dind
  rules:
    - if: $CI_COMMIT_TAG                 # Run this job when a tag is created
  variables:
    IMAGE_TAG: $CI_REGISTRY_IMAGE:$CI_COMMIT_TAG
  script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker build -t $IMAGE_TAG -t $CI_REGISTRY_IMAGE:latest .
    - docker push $IMAGE_TAG

release_job:
  stage: build
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  needs:
    - job: prepare_job
  rules:
    - if: $CI_COMMIT_TAG
  script:
    - echo "Running release_job for $CI_COMMIT_TAG"
  release:
    tag_name: '$CI_COMMIT_TAG'
    description: 'Release $CI_COMMIT_TAG'
